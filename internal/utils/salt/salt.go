package salt

import (
	"crypto/rand"
)

func NewRandom() ([]byte, error) {
	s := make([]byte, 32)
	_, err := rand.Read(s)
	if err != nil {
		return nil, err
	}
	return s, nil
}

func Must(salt []byte, err error) []byte {
	if err != nil {
		panic(err)
	}
	return salt
}

func New() []byte {
	return Must(NewRandom())
}