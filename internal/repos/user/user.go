package user

import (
	"context"
	"encoding/base64"
	"fmt"
	"gitlab.com/mts-teta-golang-practice/auth/internal/utils/salt"
	"golang.org/x/crypto/scrypt"
)

type User struct {
	Login string
	Salt string
	PasswordHash string
}

type UserStore interface {
	Create(ctx context.Context, u User) (error)
	Read(ctx context.Context, login string) (*User, error)
}

type Users struct {
	store UserStore
}

func NewUsers(s UserStore) *Users {
	return &Users{
		store: s,
	}
}

func (us *Users) Register(ctx context.Context, login string, password string) (*User, error) {

	s := salt.New()
	h, err := scrypt.Key([]byte(password), s, 32768, 8, 1, 32)
	if err != nil {
		return nil, fmt.Errorf("register user error: %w", err)
	}

	u := User{
		Login: login,
		Salt: base64.StdEncoding.EncodeToString(s),
		PasswordHash: base64.StdEncoding.EncodeToString(h),
	}

	err = us.store.Create(ctx, u)
	if err != nil {
		return nil, fmt.Errorf("register user error: %w", err)
	}

	return &u, nil
}

func (us *Users) Read(ctx context.Context, login string) (*User, error) {

	u, err := us.store.Read(ctx, login)
	if err != nil {
		return nil, fmt.Errorf("read user error: %w", err)
	}

	return u, nil
}